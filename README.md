# Repositório pessoal: Compass

Este repositório contém material elaborado pela bolsista Anniely Mariah sobre o programa AWS for Software Quality & Test Automation - Compass UOL. Aqui você encontrará breves documentações sobre conhecimento adquirido durante a jornada, além de projetos pessoais e testes elaborados ao decorrer do programa de bolsa.

## Divisão do repositório

Para organização, foi-se gerada um arquivo .md de registro diário dos conteúdos consumidos. Além disso, foi-se incluso resumos dos conteúdos das dailys, separadas em pastas para cada sprint. Assim tendo, em cada uma delas, seus respectivos 10 documentos refentes aos dias de estudo.

- Sprint 1: Processos Ágeis & AWS
- Sprint 2: Fundamentos APIs REST & Análise de Teste
- Sprint 3: Fundamentos de Testes de Performance
- Sprint 4: K6 Primeiros Passos
- Sprint 5: Medindo a performance & Reports
- Sprint 6: Docker, AWS e Challenge Final

## Objetivos pessoais

1. Adquirir conhecimento em Back-End, AWS e CyberSecurity;
2. Evoluir profissionalmente na área de tecnologia;
3. Me desenvolver e aprender a conviver dentro de uma equipe;
4. Elaborar um bom repositório.

