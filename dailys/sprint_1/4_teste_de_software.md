# Seção 2: Introdução ao teste de software

## O que é teste de software, qual sua importância e necessidade no mercado de trabalho

O teste de software é um processo com o intuito de avaliar a qualidade e desempenho do software antes de ser entregue ao cliente final. É uma etapa importante, sendo fundamental no ciclo do desenvolvimento de software. Tendo como objetivo identificar os bugs, erros e problemas que afetam a utilização do programa, a funcionalidade e segurança.

### O principal problema: BUG

O bug é um problema ocorrido em um software. Sendo causado por um erro de programação, design, entre outros. Os bugs podem apresentar diferentes impactos e manifestações, segundo Chat Openai "Alguns bugs podem causar falhas no software, levando-o a parar de funcionar completamente. Outros podem resultar em comportamentos inesperados, como resultados incorretos, travamentos, mensagens de erro confusas, lentidão do sistema ou comportamento inadequado da interface do usuário."

Os bugs podem se apresentar em qualquer fase do ciclo do desenvolvimento de um software, alguns sendo faceis de se observar e corrigir e outros sendo mais complexos e com necessidade de visão externa. Daí se enquadra os testes de software.

Principais áreas prejudicadas pelos bugs:
- Empresas e Organizações;
- População comum;
- Governos;
- Meio ambiente.

### Importância do teste de software

1. Identificação de erro ou defeitos: Defeitos podem fazer com que o projeto sofra provido de suas falhas, atrasando prazos, gerando desagrado e falta de qualidade no produto final. Permanecer com os erros em um software pode gerar perda de vendas tanto no presente momento quanto a longo prazo. 

2. Garantia de qualidade: A sua imagem e da empresa é fundamental para seu crescimento, assegurar que o produto final seja confiável, funcional e atenda às expectativas dos usuários é necessário.

3. Melhoria da usabilidade: Melhorar a performace e otimização do software é uma das coisas que um teste pode identificar, ter uma interface intuitiva, seguir padrões estabelecidos pela instituição também são fatores relevantes para usabilidade.

4. Economia de tempo e recurso: Manter um código funcional trará resultados de agrado a longo prazo. As vezes, um código "gambiarra" funciona por um momento mas trará dificuldades futuras. Por essa razão, testar e prever é __muito__ importante.

5. Segurança: O teste de software é crucial para garantir a confiabilidade e a segurança do software. Isso porque ele identifica vulnerabilidades, falhas de autenticação, problemas de integridade de dados e outras questões de segurança. Ao abordar essas preocupações no processo de teste, reduzimos os riscos de violações e protegemos os dados dos usuários.

## 7 fundamentos do Teste (ISTQB)

1. **Testes demonstram a presença de defeitos, não sua ausência.**
Testes de software tem o poder de apresentar defeitos, mas não tem a capacidade de diagnosticar a ausência de defeitos. Nós não podemos garantir o completo extermínio de defeitos! Não há como por a mão no fogo sobre a garantia 100% do software.
Outra coisa, corrigir defeitos podem gerar outros defeitos, isso não é algo incomum.
- Você testa pra prevenir transtornos.

2. **Teste exaustivo não é possível.**
- Testar todas as possibilidade não é viável, exceto em situações extremamente triviais.
- Saiba pranejar o grau de prioridade de testes, sendo categorizado pelo risco ou pela necessidade no mercado.

3. **Teste antecipado.**
Quanto mais cedo você testar algo, melhor vai ser a construção do código no ciclo do desenvolvimento. 
_"Quanto mais cedo encontrarmos o defeito, mais __*barata*__ será sua identificação."_

![10 Myers](https://media.discordapp.net/attachments/1027385935333171220/1109115713915994142/custo.png?width=571&height=389)

4. **Agrupamento de defeitos.**
Bugs estão distribuidos de forma heterogênea, alguns modulos contém muitos defeitos e alguns devem ter menos. Normalmente, algum lugar que tem mais defeitos costumam ter mais defeitos.

5. **Paradoxo do pesticida.**
O "paradoxo do pesticida" é um conceito em teste de softwares referente a uma situação paradoxal onde o controle de pragas com uso de pesticidas podem a longo prazo levar a evolução da resistência do inseto e assim ao aumento da população.
Quando um pesticida é aplicado em uma plantação, ele mata a maioria das pragas presentes no início. No entanto, algumas pragas podem ter uma resistência natural aos pesticidas ou mutações aleatórias que as tornam menos suscetíveis aos produtos químicos. Essas pragas sobreviventes têm uma vantagem reprodutiva, pois competem com menos indivíduos pela comida e recursos.

Com essa ideia em mente, levando para o contexto de software, para superar o paradoxo os testes necessitam serem frequentemente revisados. Remanejando de forma a aumentar a possibilidade de encontrar erros.

6. **Testes dependem do contexto.**
O sexto fundamento acredito eu que seja o mais intuitivo de todos. Em suam, testes são realizados de formas diferentes dependendo do contexto e necessidade.

7. **A ilusão da ausência de erros.**
A ilusão da ausência de erros é um fenômeno pelo qual o testador após remanejar e ajustar os erros de um sistema determinado, concluí que está sem aparentes erros. No entanto, o produto não atendia as expectativas dos usuários. É importante além de buscar a segurança e identificação de erros, a qualidade e usabilidade do programa. Ainda assim, não adianta entregar um software perfeito se não é mais o momento certo para o cliente. É uma questão de __necessidade do cliente__. 

## Teste X QA

Tester é aquele que avalia o produto, podendo usufruir de testes automatizados ou não para essa realização. O QA trabalha sobre as lições aprendidas, ele não foca em ajustar o produto, ele quer melhorar o processo! A forma de fazer com menos custo, com menos tempo de parada, com a gestão de melhoria do procedimento.

- Tester: Buscar bugs, fazem automação de testes.
- QA: Aplicar lições aprendidas, melhorar processo.

![QA](https://cdn.discordapp.com/attachments/1027385935333171220/1109193936922214531/image.png)


### Erro, defeito e falha
- Erros: Enganos, que produzem defeitos.
- Defeitos: Bugs no código, no sistema que se tornam uma falha.
- Falha: Momento em que o defeito é executado, gerando insatisfação do cliente.

Se eu encontrar algo que eu realizei equivocado, é um erro. Se for o do proximo, é defeito. Entretanto, o termo "defeito" gera muito atrito, é melhor utilizar ocorrência ou incidente. Caso ela confirme que está errado, é um defeito.

## Teste Manual x Teste automatizado

### Teste manual

Os testes manuais são atividades realizadas por seres humanos para avaliar um sistema ou aplicativo de software em busca de defeitos, inconsistências ou comportamentos indesejados. Eles envolvem a execução de etapas de teste, como inserção de dados, interação com a interface do usuário e verificação dos resultados. Os testes manuais requerem intervenção humana direta, permitindo a detecção de problemas que podem passar despercebidos em testes automatizados.

### Teste automatizado
Teste automatizado é um processo de construção que facilitem testar e alcançar bem grandes demanda sem a necessidade de interação humana constante, apenas para verificação. Testes automatizados ocorrem em uma base diária e detectar erros de integração de maneira veloz e antecipada.

- A integração contínua permite que testadores ágeis realizem testes automatizados regurlamente.

### Testes de regressão automatizados

Testes de regressão automatizados referem-se a atividades de teste realizadas por meio de ferramentas de automação para verificar se as alterações feitas em um sistema não introduziram novos erros ou afetaram negativamente o funcionamento existente.

- Os resultados são visíveis para todos os membros da equipe, especialmente quando os relatórios automatizados são integrados no processo.
- Podem ser contínuos ao longo da iteração.
- Abrangem a maior quantidade de funcionalidades possíveis.
- Boa cobertura nos testes de regressão automatizados ajuda o desenvolvimento (e teste) de grandes sistemas integrados.
- Ao se conseguir desenvolver um teste de regressão, possibilita com que os testadores sejam livres para se concentrar em outras atividades. (Teste de confirmação, implementação de novas funcionalidades, geração de outros testes entre outros.)

## Testes ágeis x Testes tradicionais

Os testes tradicionais e os testes ágeis diferem em seus princípios, abordagens e práticas. Aqui estão algumas diferenças-chave entre eles:

- Ciclo de vida do desenvolvimento: Os testes tradicionais seguem um modelo de desenvolvimento em cascata, onde as etapas de desenvolvimento são realizadas sequencialmente, com os testes ocorrendo após o desenvolvimento. Já os testes ágeis são realizados em ciclos iterativos e incrementais, como parte integrante do processo de desenvolvimento.

- Foco na documentação: Os testes tradicionais geralmente dependem de documentação detalhada, como especificações de requisitos e planos de teste extensos. Em contraste, os testes ágeis valorizam mais a comunicação verbal e a colaboração entre as equipes, com menos ênfase na documentação extensiva.

- Flexibilidade e adaptação: Os testes tradicionais tendem a ser rígidos, seguindo planos de teste pré-determinados. Os testes ágeis são mais flexíveis e adaptáveis, permitindo ajustes e mudanças nos casos de teste e estratégias à medida que o projeto evolui.

- Integração contínua e automação: Os testes ágeis enfatizam a integração contínua, em que as alterações de software são testadas imediatamente após a implementação. Isso é apoiado pela automação dos testes, que é amplamente utilizada nos testes ágeis para aumentar a eficiência e a velocidade dos testes.

- Colaboração e envolvimento do cliente: Os testes ágeis incentivam a colaboração constante entre desenvolvedores, testadores e stakeholders do projeto. A participação ativa do cliente é valorizada, permitindo feedback rápido e contínuo, o que ajuda a refinar os requisitos e melhorar a qualidade do software.