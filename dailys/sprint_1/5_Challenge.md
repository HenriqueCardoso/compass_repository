# Como um QA pode gerar qualidade em um projeto

## Quality Assurance: Definição

Imagine um tester que realiza mais do que o esperado, um profissional que além de se entregar para o software, também busca melhorias em seu meio trabalhistico e otimização das metodologias que lá são ultilizadas. Esse é o Quality Assurance (QA), possuindo uma visão ampla do projeto, um QA segue além da perspectiva do programa e do que ele entrega, tendo seu foco em garantir a excelência em todos os aspectos. 
Enquanto um teste comum busca apenas cumprir o que é proposto, o QA se preocupa em entregar da melhor forma possível, como em de que forma a equipe está se organizando, como tornar a produção sera mais eficiente, que maneiras podem ser feitas pra melhhorarem os custos do produto, se ele pode entregar fatores relevantes além do solicitado entre outros. 

Podemos ilustrar isso com o seguinte exemplo:

![0](https://cdn.discordapp.com/attachments/1027385935333171220/1109895529120407622/image.png)

__Eficácia:__ Um teste entrega apenas o _proposto_.

__Eficiência:__ Um QA se preocupa em entregar a _melhor solução possível_, considerando o contexto geral.

## Como um QA pode gerar qualidade em um projeto.

Aqui estão apresentadas algumas práticas que podem ajudar a garantir a qualidade em um projeto, como:

1. __Alcançar uma cobertura de testes completa:__
Para qualquer profissional no ramo de testes de software, é relevante ressaltar a importância de uma cobertura bem feita, isso não significar gerar testes desnecessário até a exaustão, e sim pensar em maneiras que possam agregar uma maior quantidade de possíveis bugs á serem solucionados.
Podemos inclusive, pontuar algumas ressalvas, como:

    - Adotar a perspectiva do usuário.
    - Priorizar métodos mais utilizados nos testes, deixando de lado os menos relevantes para evitar desperdícios de tempo e recursos (Princípio de Pareto).
    - Realizar testes o mais cedo possível durante o ciclo de produção.
    - Testar em todas as atividades do ciclo de vida do produto.
    - Revisar e atualizar regularmente os testes, adaptando-os para encontrar novas falhas. Dessa forma, evitando o "Paradoxo Pesticida".

2. __Cumprir os prazos e expectativas do cliente:__
Cumprir as expectativas do usuário, cliente, empresa ou qualquer entidade que tenha solicitado ou que usufrua de nosso software é essencial para uma boa imagem da empresa. E para conseguirmos realizar essa produção, há de ser nescessário alguns métodos, por exemplo:

    - Manter uma comunicação eficaz e que compreenda as necessidades futuras.
    - Estabelecer uma lista de prioridades clara.
    - Gerenciar o tempo de forma eficiente.

Afinal, garantir a entrega dentro do prazo e atender às expectativas do cliente são fundamentais para o sucesso do projeto.

![1](https://cdn.discordapp.com/attachments/1027385935333171220/1109900663300837467/image.png)

3. __Compreensão da psicologia do teste:__
Saindo da pespectiva de entrega ao usuário e adentrando a própria comunicação na equipe, fornecer bons feedbacks e manter uma comunicação clara é essencial para um bom desenvolvimento. Mas claro, existem momentos e situações que dificultam essa atrelação entre os funcionários, principalmente quando falamos de testes. Os profissionais que são responsáveis por estes setor, vivem constantemente tendo que auxiliar e até "desfazer" códigos de seus colegas. E, dependendo da forma que se é comunicado, a relação entre eles pode se tornar desagradável. Portanto:

- Entender a diferença entre __erros, defeitos e falhas__:

    Erro: ação humana que leva a um mau entendimento ou execução inadequada (por exemplo, falta de entendimento de como executar um cálculo).
    Defeito: resultado de um erro de entendimento, como um código com uma fórmula de cálculo mal escrita.
    Falha: a execução de um defeito que leva a resultados incorretos (por exemplo, um cálculo que produz resultados errados).
    
- Usar termos como "ocorrência" ou "incidente" para comunicar defeitos/erros, pode evitar interpretações negativas.

- Além disso, é pertinênte estar ciente do viés de confirmação, que nos leva a enxergar mais aquilo em que acreditamos e concordamos, em vez de observar o que discordamos.

![2](https://media.discordapp.net/attachments/1027385935333171220/1109906920980107397/image.png?width=923&height=499)

![3](https://media.discordapp.net/attachments/1027385935333171220/1109898677205602404/image.png?width=979&height=596)

![4](https://media.discordapp.net/attachments/1027385935333171220/1109898760470941756/image.png?width=830&height=535)

4. __Um software sem erros não é uma boa razão para lançar em produção:__

    - A ausência de erros pode indicar que eles estão ocultos ou que o software não foi suficientemente testado.
    - Lembre-se de que lançar um software sem erros não garante sua aceitação pelos usuários. Portanto, é fundamental realizar testes de usabilidade.

5. __Encontrar e corrigir defeitos não é útil se:__

    - O produto não for utilizável.
    - O produto não atender às necessidades e expectativas dos usuários.

6. __O teste de software não é um processo aleatório para descobrir bugs:__
A realização de testes é um processo estruturado para garantir sua eficacia, considerando as necessidades do produto.

7. __Estar sempre em busca de inovação nos testes:__
Estar sempre em busca de inovação nos testes de software é essencial para acompanhar o ritmo acelerado das mudanças tecnológicas e garantir a qualidade dos produtos e sistemas de software. A constante evolução das tecnologias e das necessidades dos usuários demanda abordagens cada vez mais sofisticadas e eficientes para testar e validar o software.
Portanto, faz-se nescessário:

    - Explorar novas técnicas e abordagens para aprimorar a qualidade do software.
    - Se manter atualizado sobre as melhores práticas e tendências da área.

Ao seguir essas práticas, um QA pode desempenhar um papel fundamental na geração de qualidade excepcional em um projeto de software, garantindo a satisfação do cliente e o sucesso do produto.

# Estudo de caso hipotético

Imaginando um cenário real em que essas ideias são aplicadas em um ambiente ágil, como em projetos do segmento de varejo. Nesse contexto, a equipe geralmente trabalha de forma colaborativa e iterativa, buscando entregar valor ao cliente de maneira rápida e eficiente. 
Vejamos como as práticas do QA podem ser aplicadas nesse cenário:

1. __Alcancar uma cobertura de testes completa:__
Em um ambiente ágil, é essencial ter uma cobertura de testes abrangente para garantir a qualidade do software. A equipe de QA pode trabalhar em estreita colaboração com os desenvolvedores para identificar os principais fluxos de trabalho e funcionalidades críticas do sistema de varejo, priorizando testes nesses aspectos. Por isso:

    - Os testes devem ser realizados o mais cedo possível durante o ciclo de desenvolvimento ágil, garantindo que os defeitos sejam identificados e corrigidos rapidamente.
    - A equipe de QA deve acompanhar o progresso do projeto e garantir que os testes sejam executados em todas as etapas do ciclo de vida do produto, incluindo integração contínua, testes de unidade, testes de sistema e testes de aceitação.

2. __Cumprir prazos e expectativas do cliente:__

    - A comunicação e colaboração eficazes são cruciais para entender as necessidades do cliente no setor de varejo. A equipe de QA deve manter uma comunicação constante com os stakeholders e o cliente para entender as prioridades e garantir que as entregas estejam alinhadas com as expectativas.
    - A equipe ágil deve estabelecer uma lista de prioridades clara, identificando os recursos e funcionalidades mais importantes para o sucesso do projeto de varejo. Isso ajudará a garantir que o tempo seja gerenciado adequadamente e que os requisitos essenciais sejam atendidos.

3. __A psicologia do teste:__
Em um ambiente ágil, é essencial fornecer feedback contínuo e efetivo para a equipe de desenvolvimento. A equipe de QA deve colaborar de perto com os desenvolvedores, compartilhando informações sobre defeitos, ocorrências e falhas encontradas nos testes. Portanto:

    - É importante utilizar uma linguagem clara e não ambígua ao relatar defeitos e erros, evitando mal-entendidos e facilitando a compreensão mútua. Isso contribui para uma colaboração mais eficiente entre a equipe de QA e a equipe de desenvolvimento.
    - Além disso, a equipe de QA deve estar ciente do viés de confirmação e trabalhar ativamente para superá-lo, garantindo que todos os cenários de teste relevantes sejam considerados, mesmo aqueles que possam desafiar as suposições prévias.

4. __Não se contentar com um software livre de erros:__

    - Em projetos de varejo, em particular, é crucial realizar testes de usabilidade para garantir que o software atenda às necessidades dos usuários finais. A equipe de QA deve se concentrar em avaliar a facilidade de uso, a navegabilidade e a experiência do usuário durante todo o processo de teste.
    - Além disso, mesmo que o software esteja aparentemente livre de erros, é importante realizar testes de carga e desempenho.
