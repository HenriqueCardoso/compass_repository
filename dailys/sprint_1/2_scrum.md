# SCRUM e derivados

## Manifesto ágil

O Manifesto Ágil, também conhecido como Manifesto para o Desenvolvimento Ágil de Software, é um documento que estabelece os valores e princípios fundamentais do desenvolvimento ágil de software. Foi criado em 2001 por um grupo de profissionais da área de software que buscavam uma abordagem mais flexível e colaborativa para o desenvolvimento de projetos.

O manifesto é composto por quatro valores e doze princípios que orientam a filosofia ágil. Os valores são:

1. Indivíduos e interações acima de processos e ferramentas: Valoriza-se a colaboração e a comunicação efetiva entre as pessoas envolvidas no projeto, reconhecendo que o relacionamento e a interação são essenciais para o sucesso.

2. Software funcionando acima de documentação abrangente: Valoriza-se a entrega de software funcional e de valor para o cliente, priorizando a prática constante de testes e iterações.

3. Colaboração com o cliente acima de negociação de contratos: Valoriza-se a parceria próxima com o cliente, buscando uma colaboração contínua e interativa ao longo do processo de desenvolvimento.

4. Responder a mudanças acima de seguir um plano: Valoriza-se a capacidade de adaptação e flexibilidade frente a mudanças de requisitos e prioridades, reconhecendo que a agilidade é essencial para lidar com a incerteza e as necessidades em constante evolução.

![Manifesto ágil](https://media.discordapp.net/attachments/1027385935333171220/1108108467866173481/image.png?width=1440&height=590)

## SCRUM: Conceituação

Scrum é um framework ágil utilizado para gerenciar projetos complexos de desenvolvimento de software e outras atividades. Ele foi criado para fornecer uma estrutura flexível que permite que equipes autogerenciadas trabalhem de forma colaborativa para entregar resultados de alta qualidade.

O Scrum se baseia em princípios de transparência, inspeção e adaptação. Ele divide o trabalho em incrementos menores e gerenciáveis chamados de "sprints". Cada sprint tem uma duração fixa, geralmente de uma a quatro semanas, durante a qual a equipe se concentra em um conjunto de itens de trabalho selecionados do backlog do produto.

Os papéis principais no Scrum incluem:

1. Product Owner: É o responsável por definir a visão do produto, priorizar o backlog do produto e garantir que a equipe esteja desenvolvendo o que é mais valioso para o negócio. _PS: Foi o que eu mais gostei. :)_

2. Scrum Master: É o facilitador do processo Scrum. O Scrum Master ajuda a equipe a entender e adotar os princípios e práticas do Scrum, remove impedimentos e promove um ambiente de trabalho colaborativo.

3. Equipe de Desenvolvimento: É o grupo de pessoas responsável por desenvolver o produto. A equipe é multidisciplinar e auto-organizada, sendo responsável por planejar, realizar e entregar os incrementos de trabalho durante os sprints.

Durante um sprint, a equipe realiza **reuniões diárias** de sincronização chamadas de **"Daily Scrum"** para discutir o progresso, os desafios e o planejamento para o próximo dia de trabalho. No final de cada sprint, ocorre uma revisão do sprint, onde o trabalho concluído é demonstrado e feedback é obtido. Em seguida, há uma retrospectiva do sprint, onde a equipe reflete sobre o processo e identifica melhorias para os próximos sprints.

O Scrum é conhecido por sua abordagem iterativa e incremental, que permite que os projetos se adaptem rapidamente às mudanças de requisitos e prioridades. É amplamente utilizado na indústria de desenvolvimento de software, mas também pode ser aplicado em outros contextos onde a abordagem ágil é benéfica.

- Site pra gerar um SCRUM que eu utilizei: https://app.proj4.me/projects/1/panels?nump=1&tamp=100&ordenacao=1&concluidos=true&queryUpdate=true

![SCRUM que eu gerei](https://media.discordapp.net/attachments/1027385935333171220/1108145423807426670/image.png?width=1433&height=666)

## O que é um Framework?

Um framework é um conjunto de conceitos, práticas e ferramentas que fornecem uma estrutura para desenvolver e executar aplicativos, sistemas ou soluções em um determinado domínio. Ele oferece uma abordagem estruturada para resolver problemas comuns e acelerar o processo de desenvolvimento, fornecendo componentes e funcionalidades reutilizáveis.

Os frameworks são projetados para facilitar o desenvolvimento de software, fornecendo uma base sobre a qual os desenvolvedores podem construir suas aplicações. Eles podem incluir bibliotecas, APIs (interfaces de programação de aplicativos), conjuntos de ferramentas, padrões de projeto e diretrizes de boas práticas.

