# Git: Conceituação básica e comandos

## O conceito de controle de versão

Controle de versão é um sistema ou método utilizado para gerenciar e acompanhar as alterações feitas em um conjunto de arquivos ou um projeto ao longo do tempo. Ele é especialmente útil em projetos de desenvolvimento de software, onde várias pessoas podem estar trabalhando simultaneamente no mesmo código-fonte.

* Histórico: Alterar entre versões ou restaurar versões anteriores.
* Branches: Possibilidade de criar diferentes versões e mescla-las.
* Rastreabilidade: Identificar mudanças realizadas.

## Tipos de controle de versão

### Centralizado

Esses sistemas possuem um repositório central onde todos os arquivos e suas versões são armazenados. Os desenvolvedores trabalham em suas cópias locais dos arquivos e **sincronizam** suas alterações com o repositório central. Exemplos de sistemas centralizados são o Subversion (SVN) e o Team Foundation Version Control (TFVC).

- Um servidor contém todo o histórico;
- Qualquer problema impacta no código, uma única versão.

### Distribuido

Nesses sistemas, cada desenvolvedor tem uma cópia completa do repositório, incluindo todas as versões dos arquivos. Isso permite que os desenvolvedores trabalhem offline e tenham maior **flexibilidade** para criar e mesclar ramificações (branches) independentes. Exemplos de sistemas distribuídos são o Git e o Mercurial.

- Toda máquina possuí uma cópia;
- Mudanças realizadas localmente;
- Não depende de uma só máquina.

## Características do Git

Ações locais.
- Navegação de histórico;
- Criação de branch.

Desenvolvimento **não-linear**.
- Varias versões;
- Facilidade em alterar arquivos sem dependência.

Integridade.
- Uma vez adicionado, o histórico deixa registrado.

Indepêndencia da complexibilidade do projeto.
- Grande, pequeno, simples ou complexo;
- Git é portável para tudo.

### Alguns comandos básicos do Git

Para configuração incial, é utilizado os seguintes comandos:
```
git config --global user.name "Nome"
git config --global user.email "Email"
```
Ao ser configurado, não há necessidade de configura-lo novamente, as informações foram _globais_, ou seja, configuradas no cerne do Git.

Outros comandos:

```
git init (Iniciar um repositório)
git add . (Rastrear todos os arquivos)
git rn --cached . (Remover o rastreamento do git sem apagar da máquina)
git commit -m "Mensagem" (Declarar como concluído e armazenar)
git push -u origin main ("Empurrar" para o repositório online)
```

### Estados do Git

No Git, existem três principais estados pelos quais os arquivos podem passar: "untracked" (não rastreado), "unmodified" (não modificado) e "modified" (modificado). Além desses estados, o Git também tem o estado "staged" (preparado).

1. Untracked (Não rastreado): Nesse estado, o Git não está acompanhando as alterações do arquivo. Isso ocorre quando um arquivo é adicionado pela primeira vez em um repositório Git ou quando um arquivo existente é removido do controle de versão.

2. Unmodified (Não modificado): Os arquivos que estão no repositório Git e não sofreram alterações após o último commit estão no estado "unmodified". Esses arquivos não foram modificados desde a última confirmação e estão em seu estado original.

3. Modified (Modificado): Quando um arquivo é alterado após o último commit, ele entra no estado "modified". Isso significa que as alterações foram feitas no arquivo, mas ainda não foram registradas em um novo commit.

4. Staged (Preparado): O estado "staged" ocorre quando um arquivo modificado é adicionado à área de preparação (staging area) do Git. A área de preparação é uma área intermediária onde as alterações são reunidas antes de serem confirmadas em um novo commit. Os arquivos preparados para o commit estão prontos para serem incluídos na próxima confirmação.

Esses estados permitem que você gerencie e controle as alterações em seu repositório Git, facilitando o acompanhamento das modificações e a criação de commits coesos.
Para identificar o estado do seu arquivo, utiliza-se do comando:
```
git status
```

### Material extra utilizado

1. Saiba mais sobre o Git: https://www.atlassian.com/br/git/tutorials/what-is-version-control

2. Sintaxe básica de gravação e formatação no GitHub: https://docs.github.com/pt/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax#uploading-assets

3. Como usar Git Lab na prática: https://www.youtube.com/watch?v=un8CDE8qOR8
4. Error: failed to push some refs to – How to Fix in Git: https://www.freecodecamp.org/news/error-failed-to-push-some-refs-to-how-to-fix-in-git/
